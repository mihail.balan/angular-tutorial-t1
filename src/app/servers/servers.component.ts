import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  yolo = 'yolo';
  serverStatus = 'no server created';
  serverCreated = false;

  servers = ['server α', 'server β'];

  constructor() {
    setTimeout(() => (this.allowNewServer = true), 2000);
  }

  ngOnInit() {}

  onCreateServer() {
    this.serverCreated = true;
    this.serverStatus = 'server created';
    this.servers.push(this.yolo);
  }

  onUpdateServerName(event: Event) {
    this.yolo = (event.target as HTMLInputElement).value;
  }
}
